/* 
Copyright (c) 2015, Glenn Ruben Bakke
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

  3. The name of the author may not be used to endorse or promote
     products derived from this software without specific prior
     written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

function _start_color()
{
	colors = new Array();
	colors.push({"f": "#FFFFFF", "b": "#000000"});
}

function _init_pair(pair, f, b)
{
	colors.splice(pair, 0, {"f": f, "b": b});
}

function _bkgdset(chtype)
{
	background = colors[chtype];
}
 
function _mvaddstr(y, x, str)
{
	ctx.beginPath();
	ctx.fillStyle = background["b"];
	ctx.fillRect(x*stdscr.font_width, 
		         y*stdscr.font_size, 
		         stdscr.font_width*str.length,
		         stdscr.font_size);
	
	ctx.fillStyle = background["f"];
	ctx.font = "bold " + stdscr.font_size + "px Courier New";
	for (var i = 0; i < str.length; i++)
	{
		ctx.fillText(str[i],(x+i) * stdscr.font_width,(y + 1) * stdscr.font_size - stdscr.font_size / 4);
	}
	ctx.stroke();
	ctx.closePath();
} 

var key_pressed = null;

function func_read_key(ev)
{
	key_pressed = ev.keyCode
}

function _getch()
{
	
	document.addEventListener("keydown", func_read_key, true);

	while (key_pressed == null)
	{
		;
	}

	document.removeEventListener("keydown", func_read_key);

	return 0;
}

var ctx = null;
var stdscr;
var colors = null;
var background = null;


var COLOR_BLACK   = "#000000";
var COLOR_BLUE    = "#0000FF";
var COLOR_CYAN    = "#00FFFF";
var COLOR_GREEN   = "#00FF00";
var COLOR_MAGENTA = "#FF00FF";
var COLOR_RED     = "#FF0000";
var COLOR_WHITE   = "#FFFFFF";
var COLOR_YELLOW  = "#FFFF00";

function initscr()
{
	var c = document.getElementById("webcurses");

	ctx = c.getContext("2d");
	var font_size = 18;
	var font_width = font_size * 6 / 10;

	c.height = c.height * font_size;
	c.width = c.width * font_width;

	var sizeWidth = ctx.canvas.clientWidth;
	var sizeHeight = ctx.canvas.clientHeight;

	stdscr = {
		num_lines: ~~(sizeHeight / font_size),
		num_rows: ~~(sizeWidth / font_width),
		font_size: font_size,
		font_width: font_width,
		mvaddstr:    _mvaddstr,
		start_color: _start_color,
		init_pair:   _init_pair,
		bkgdset:     _bkgdset,		
		getch:       _getch,
	}

	ctx.fillStyle = COLOR_BLACK;
	ctx.fillRect(0,0,sizeWidth,sizeHeight);

	return stdscr;
}
